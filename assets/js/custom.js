$(document).ready(function(){
	setTimeout(function(){
		$('#messageBox').modal('show');
	},30000); 

	$( "#msg-box" ).submit(function( event ) {

		$(".notf").show();
		$(".submit-btn").hide();

		event.preventDefault();
		let form = $(this);
    	
		$.ajax({
			type: "POST",
			url: 'config.php',
			data: form.serialize(),
			success: function(response) {
				$(".notf").hide();
				$(".msg").removeClass('alert alert-success');
				$(".msg").removeClass('alert alert-danger');
				if(response == 200) {
					$(".msg").addClass('alert alert-success');
					$(".msg").text("Form submitted successfully!");
					$(".msg").show();
					$("form").trigger("reset");
					$(".submit-btn").show();
				} else if(response === 'required') {
					$(".msg").addClass('alert alert-danger');
					$(".msg").text("All fields are required");
					$(".msg").show();
					$(".submit-btn").show();
				} else {
					$(".msg").addClass('alert alert-danger');
					$(".msg").text("Error! form not submitted, please try again");
					$(".msg").show();
					$(".submit-btn").show();
				}
			},
			error: function(data) {
				$(".notf").hide();
				$(".submit-btn").show();
				$(".msg").addClass('alert alert-danger');
				$(".msg").text("Error! form not submitted, please try again");
			}
		});
	});

	// Partners Toggle
	$('#partners_img').hide();
	$('#partners_toggle').click(function() {
		$('#partners_img').slideToggle('slow');
 });
 
 // Media Button
	$('.media-btn ul li').click(function() {
  $(this).addClass('active');
		$(this).siblings('li').not($(this)).removeClass('active');
 });
 
 $('#all_media').click(function() {
  $('.media-content .media-card').show('slow');
 });
 $('#video_media').click(function() {
  $('.media-content .media-card').hide('slow');
  $('.media-content .media-card.video').show('slow');
 });
 $('#pdf_media').click(function() {
  $('.media-content .media-card').hide('slow');
  $('.media-content .media-card.pdf').show('slow');
 });
	
});
